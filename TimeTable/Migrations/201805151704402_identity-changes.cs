namespace TimeTable.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class identitychanges : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.TimeTables", newName: "Schedules");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.Schedules", newName: "TimeTables");
        }
    }
}
