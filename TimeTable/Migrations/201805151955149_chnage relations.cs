namespace TimeTable.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class chnagerelations : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Courses", "Course_Id", "dbo.Courses");
            DropForeignKey("dbo.LecturerCourses", "CourseId", "dbo.Courses");
            DropForeignKey("dbo.LecturerCourses", "LecturerId", "dbo.Lecturers");
            DropIndex("dbo.Courses", new[] { "Course_Id" });
            DropIndex("dbo.LecturerCourses", new[] { "CourseId" });
            DropIndex("dbo.LecturerCourses", new[] { "LecturerId" });
            RenameColumn(table: "dbo.StudentCourses", name: "CourseId", newName: "ScheduleId");
            RenameIndex(table: "dbo.StudentCourses", name: "IX_CourseId", newName: "IX_ScheduleId");
            AddColumn("dbo.Schedules", "TotalHours", c => c.Int(nullable: false));
            AddColumn("dbo.Students", "RowVersion", c => c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"));
            DropColumn("dbo.Courses", "Course_Id");
            DropTable("dbo.LecturerCourses");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.LecturerCourses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CourseId = c.Int(nullable: false),
                        LecturerId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Courses", "Course_Id", c => c.Int());
            DropColumn("dbo.Students", "RowVersion");
            DropColumn("dbo.Schedules", "TotalHours");
            RenameIndex(table: "dbo.StudentCourses", name: "IX_ScheduleId", newName: "IX_CourseId");
            RenameColumn(table: "dbo.StudentCourses", name: "ScheduleId", newName: "CourseId");
            CreateIndex("dbo.LecturerCourses", "LecturerId");
            CreateIndex("dbo.LecturerCourses", "CourseId");
            CreateIndex("dbo.Courses", "Course_Id");
            AddForeignKey("dbo.LecturerCourses", "LecturerId", "dbo.Lecturers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.LecturerCourses", "CourseId", "dbo.Courses", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Courses", "Course_Id", "dbo.Courses", "Id");
        }
    }
}
