namespace TimeTable.Migrations
{
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Threading.Tasks;
    using TimeTable.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<TimeTable.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }
       
        protected override void Seed(ApplicationDbContext context)
        {
             SeedUsers(context).Wait();
        }

        private async Task SeedUsers(ApplicationDbContext context)
        {
            var userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(context));

            var roles = new string[] { "Admin", "Lecturer", "Student" };
            foreach (var item in roles)
            {
                context.Roles.AddOrUpdate(a => a.Name, new IdentityRole()
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = item
                });
                await context.SaveChangesAsync();
            }

            var defaultUsers = new string[] { "admin@users.com", "lecturer@users.com", "student@users.com" };
            foreach (var item in defaultUsers)
            {
                if (!context.Users.Any(a => a.UserName == item))
                {
                    var user = new ApplicationUser() {
                        Id = Guid.NewGuid().ToString() , UserName = item , Email = item , Name = item.Substring(0,item.IndexOf("@"))
                    };
                    var createUser = await userManager.CreateAsync(user , "123456");
                    if (createUser.Succeeded)
                    {
                        switch (user.Name)
                            {
                            case "admin": await userManager.AddToRoleAsync(user.Id, "Admin");
                                break;
                            case "student":
                                await userManager.AddToRoleAsync(user.Id, "Student");
                                break;
                            case "lecturer":
                                await userManager.AddToRoleAsync(user.Id, "Lecturer");
                                break;
                        }
                    }

                }
            }
        }

    }
}
