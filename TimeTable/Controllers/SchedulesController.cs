﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TimeTable.Models;
using TimeTable.Models.DomainModels;

namespace TimeTable.Controllers
{
    public class SchedulesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Schedules
        public ActionResult Index()
        {
            var timeTables = db.TimeTables.Include(s => s.Course).Include(s => s.Lecturer).Include(s => s.Room);
            return View(timeTables.ToList());
        }

        // GET: Schedules/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Schedule schedule = db.TimeTables.Find(id);
            if (schedule == null)
            {
                return HttpNotFound();
            }
            return View(schedule);
        }

        // GET: Schedules/Create
        public ActionResult Create()
        {
            PrepareSchedule(null);
            return View();
        }

        // POST: Schedules/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,CourseId,LecturerId,DateAndTime,RoomId")] Schedule schedule)
        {
            if (ModelState.IsValid)
            {
                db.TimeTables.Add(schedule);

                db.SaveChanges();
                PrepareSchedule(schedule);
                return RedirectToAction("Index");
            }

           
            return View(schedule);
        }


        private void PrepareSchedule(Schedule schedule)
        {
            ViewBag.CourseId = new SelectList(db.Courses.Select(a=>new { Id = a.Id , Code = a.Code+"-"+a.Title }), "Id", "Code", schedule?.CourseId);
            ViewBag.LecturerId = new SelectList(db.Lecturers.Select(a => new { Id = a.Id, Code = a.Code + "-" + a.FullName }), "Id", "Code", schedule?.LecturerId);
            ViewBag.RoomId = new SelectList(db.Rooms, "Id", "Code", schedule?.RoomId);
        }

        // GET: Schedules/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Schedule schedule = db.TimeTables.Find(id);
            if (schedule == null)
            {
                return HttpNotFound();
            }
            PrepareSchedule(schedule);
            return View(schedule);
        }

        // POST: Schedules/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,CourseId,LecturerId,DateAndTime,RoomId")] Schedule schedule)
        {
            if (ModelState.IsValid)
            {
                db.Entry(schedule).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            PrepareSchedule(schedule);

            return View(schedule);
        }

        // GET: Schedules/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Schedule schedule = db.TimeTables.Find(id);
            if (schedule == null)
            {
                return HttpNotFound();
            }
            return View(schedule);
        }

        // POST: Schedules/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Schedule schedule = db.TimeTables.Find(id);
            var remove2 = db.StudentCourses.Where(a => a.ScheduleId == schedule.Id).FirstOrDefault();
            db.StudentCourses.Remove(remove2);
            db.TimeTables.Remove(schedule);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
