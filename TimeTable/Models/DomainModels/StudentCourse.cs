﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TimeTable.Models.DomainModels
{
    public class StudentCourse
    {
        [Key]
        public int Id { get; set; }

        [Display(Name ="Course")]
        [Required]
        public int ScheduleId { get; set; }

        [Display(Name = "Student")]
        [Required]
        public int StudentId { get; set; }

        [Display(Name = "Year")]
        [Required]
        public string Year { get; set; }

        [Display(Name = "Semester")]
        [Required]
        public string SemesterName { get; set; }

        [ForeignKey("ScheduleId")]
        [Required]
        public virtual Schedule Course { get; set; }

        [ForeignKey("StudentId")]
        [Required]
        public virtual Student Student { get; set; }
    }
}