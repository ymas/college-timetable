﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TimeTable.Models.DomainModels
{
    public class Schedule
    {
        public Schedule()
        {
            this.StudentCourses = new HashSet<StudentCourse>();
        }
        [Key]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Course Name")]
        public int CourseId { get; set; }

        [Display(Name = "Lecturer")]
        [Required]
        public int LecturerId { get; set; }


        [Display(Name = "Date/Time")]
        [Required]
        public DateTime DateAndTime { get; set; }

        [Display(Name = "Hours")]
        [Required]
        public int? TotalHours { get; set; }

        [Display(Name = "Room")]
        [Required]
        public int RoomId { get; set; }

        [ForeignKey("CourseId")]
        public virtual Course Course { get; set; }

        [ForeignKey("LecturerId")]
        public virtual Lecturer Lecturer { get; set; }

        [ForeignKey("RoomId")]
        public virtual Room Room { get; set; }

        public virtual ICollection<StudentCourse> StudentCourses { get; set; }


    }
}