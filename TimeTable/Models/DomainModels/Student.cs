﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TimeTable.Models.DomainModels
{
    public class Student
    {
        public Student()
        {
            this.Courses = new HashSet<StudentCourse>();
        }
        [Key]
        public int Id { get; set; }

        [Display(Name ="Student Code")]
        [Required]
        public string Code { get; set; }

        [Display(Name ="Full Name")]
        [Required]
        [ConcurrencyCheck]
        public string FullName { get; set; }

        [EmailAddress]
        [Required]
        public string Email { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }

        [Required]
        [Display(Name ="Register Date")]
        public DateTime RegisterDate { get; set; }

        [Display(Name = "User Account")]
        public string UserId { get; set; }

        [ForeignKey("UserId")]
        public virtual ApplicationUser ApplicationUser { get; set; }

        [CascadeDelete]
        public virtual ICollection<StudentCourse> Courses { get; set; }
    }
}