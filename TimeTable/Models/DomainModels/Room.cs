﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TimeTable.Models.DomainModels
{
    public class Room
    {
        public Room()
        {
            this.Schedules = new HashSet<Schedule>();
        }
        [Key]
        public int Id { get; set; }

        [Required]
        public string Code { get; set; }

        [Display(Name ="Name")]
        [Required]
        public string Name { get; set; }

        public virtual ICollection<Schedule> Schedules { get; set; }


    }
}