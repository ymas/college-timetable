﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TimeTable.Models.DomainModels
{
    public class Lecturer
    {
       

        [Key]
        public int Id { get; set; }

        [Required]
        public string Code { get; set; }

        [Display(Name ="Full Name")]
        [Required]
        public string FullName { get; set; }

        [EmailAddress]
        [Required]
        public string Email { get; set; }

        [Display(Name ="User Account")]
        public string UserId { get; set; }

      

    }
}