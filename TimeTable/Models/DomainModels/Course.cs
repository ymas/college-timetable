﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TimeTable.Models.DomainModels
{
    public class Course
    {
        public Course()
        {
            this.Schedules = new HashSet<Schedule>();
        }

        [Key]
        public int Id { get; set; }

        [Required]
        public string Code { get; set; }

        [Display(Name ="Title")]
        [Required]
        public string Title { get; set; }

        public virtual ICollection<Schedule> Schedules { get; set; }

    }
}