﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TimeTable.Models.DomainModels;

namespace TimeTable.Models.ModelViews
{
    public class CreateCourseModelView
    {
        public int Id { get; set; }
        [Required]
        public string Code { get; set; }
        [Required]
        public string Title { get; set; }
        [Display(Name ="Assign Lecturers")]
        public List<CouruseSelectedLecturers> Lecturers { get; set; }
        public int?[] SelectedLecturers { get; set; }
    }

    public class LectureSelectedCourses
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public bool Selected { get; set; }
    }


    public class CouruseSelectedLecturers
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public bool Selected { get; set; }
    }
}